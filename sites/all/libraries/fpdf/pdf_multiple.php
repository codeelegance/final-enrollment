<?php

//include fpdf libraries
include('fpdf.php'); 
include('fpdi.php'); 

// set the sourcefile 
$pdf_template = '_inc/pdf/individual_AL.pdf';

$pages = 14;

$pdf = new FPDI();
   
// add a page 
$pdf->AddPage();

$pdf->setSourceFile($pdf_template); 

$tplidx = $pdf->importPage(1);
//looping through the pdf pages to add content after page 1 
for ($i = 1; $i < $pages; $i++) {
	$tplidx = $pdf->importPage($i); 
    $pdf->useTemplate($tplidx, 10, 10, 200);
    $pdf->AddPage();

    if($i==11) {
            //policy holder
            $pdf->SetXY(46, 68.5);
            $pdf->Write(1, $_SESSION['a_first_name']." ".$_SESSION['a_last_name']);
            
            //policy period
            $StartingDate = $_SESSION['a_active_date'];
            $newEndingDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($StartingDate)) . " + 1825 day"));
            $pdf->SetXY(46, 76);
            $pdf->Write(1, $StartingDate ." - ". $newEndingDate);

            //policy number
            $pdf->SetXY(48, 60.5);
            $pdf->Write(1, $_SESSION['a_user_ID']);

            //effective date
            $pdf->SetXY(48, 53);
            $pdf->Write(1, $_SESSION['a_active_date']);
            
             //issue date
            $pdf->SetXY(43, 92.5);
            $pdf->Write(1, $_SESSION['a_active_date']);         
      }
}
$pdf->Output('pdf/chubbtest/policy'. $_SESSION['a_user_ID'] . '.pdf', 'F'); 