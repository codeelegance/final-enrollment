<?php

include_once('theme/theme.inc');

function bare_css_alter(&$css) {
  $exclude = array(
    'modules/aggregator/aggregator.css' => FALSE,
    'modules/block/block.css' => FALSE,
    'modules/book/book.css' => FALSE,
    'modules/comment/comment.css' => FALSE,
    'modules/dblog/dblog.css' => FALSE,
    'modules/file/file.css' => FALSE,
    'modules/filter/filter.css' => FALSE,
    'modules/forum/forum.css' => FALSE,
    'modules/help/help.css' => FALSE,
    'modules/node/node.css' => FALSE,
    'modules/openid/openid.css' => FALSE,
    'modules/poll/poll.css' => FALSE,
    'modules/profile/profile.css' => FALSE,
    'modules/search/search.css' => FALSE,
    'modules/statistics/statistics.css' => FALSE,
    'modules/syslog/syslog.css' => FALSE,
    'modules/system/maintenance.css' => FALSE,
    'modules/system/system.maintenance.css' => FALSE,
    'modules/system/system.messages.css' => FALSE,
    'modules/taxonomy/taxonomy.css' => FALSE,
    'modules/tracker/tracker.css' => FALSE,
    'modules/update/update.css' => FALSE,
    'modules/user/user.css' => FALSE,
  );

  $css = array_diff_key($css, $exclude);
}

/**
 * Use theme_status_messages to add some additional
 *  classes that relate to Gumby
 */
function bare_status_messages($variables) {
  $display = $variables['display'];
  $output  = '';

  $status_heading = array(
    'status'  => t('Status message'),
    'error'   => t('Error message'),
    'warning' => t('Warning message'),
  );

  $gumby_config = array(
    'status'  => 'success',
    'error'   => 'danger',
    'warning' => 'warning',
  );

  $output .= '<div class="row messages">'; // Start row
  foreach (drupal_get_messages($display) as $type => $messages) {

    // Setup the gumby class to add to the wrapper
    $gumby_class = $gumby_config[$type];

    $output .= "<div class=\"messages__content twelve columns special\">\n"; // Start message content
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }

    $output .= " <ul>\n";
    foreach ($messages as $message) {
      $output .= "<li class=\"alert $gumby_class $type\">" . $message . "</li>\n";
    }
    $output .= " </ul>\n";


    $output .= "</div>\n"; // End message content
  }
  $output .= '</div>'; // End row

  return $output;
}

/**
 *Node Preprocess
 *  We're using this to grab the correct template, CSS, and JS support files.
 *  At some point, this will be expanded with "refining" files.
 **/
function bare_preprocess_node(&$vars) {
  $theme_path = drupal_get_path('theme', 'bare');
  $node = $vars['node'];

  if (($node->type == 'enrollment_path') || ($node->type == 'verification_path') || ($node->type =='page')){
    //$url = explode("/", request_path());
    //$track = isset($url[4]) ? $url[4] : '';
    $template = $node->field_template['und'][0]['value'];

    $vars['theme_hook_suggestions'][] =  'node__' . $template; // . "-" . $round;

    //Add the CSS and JS
    $css = $node->field_css['und'][0]['value'];
    $js = $node->field_javascript['und'][0]['value'];
    if (file_exists($theme_path . '/css/' .$css . '.css')) {
      drupal_add_css($theme_path . '/css/' . $css . '.css', array('type'=>'file', 'group'=>CSS_THEME, 'weight'=>10));
    }
    if (file_exists($theme_path . '/js/' .$js . '.js')) {
      drupal_add_js($theme_path . '/js/' . $js . '.js', array('type'=>'file', 'group'=>JS_THEME, 'weight'=>10));
    }

    $path = explode("/", current_path());
    $nid = $path[1];
    $enrollment = array('enrollmentNid'=>$nid);
    drupal_add_js($enrollment, 'setting');

    if($_SESSION['lists'] != null){
        $vars['session_agentid'] = $_SESSION['lists']['agentid'];
    }
    //enrollment plan tiered info
      $vars['tiered_plans'] = [
          'field_18_29_plan' =>     $node->field_plans['und'][0]['node']->field_field_18_29_plan['und'][0]['value'],
          'field_18_29_price' =>     $node->field_plans['und'][0]['node']->field_field_18_29_price['und'][0]['value'],
          'field_18_29_id' =>     $node->field_plans['und'][0]['node']->field_field_18_29_id['und'][0]['value'],
          'field_30_39_plan' =>     $node->field_plans['und'][0]['node']->field_field_30_39_plan['und'][0]['value'],
          'field_30_39_price' =>     $node->field_plans['und'][0]['node']->field_field_30_39_price['und'][0]['value'],
          'field_30_39_id' =>     $node->field_plans['und'][0]['node']->field_field_30_39_id['und'][0]['value'],
          'field_40_49_plan' =>     $node->field_plans['und'][0]['node']->field_field_40_49_plan['und'][0]['value'],
          'field_40_49_price' =>     $node->field_plans['und'][0]['node']->field_field_40_49_price['und'][0]['value'],
          'field_40_49_id' =>     $node->field_plans['und'][0]['node']->field_field_40_49_id['und'][0]['value'],
          'field_50_59_plan' =>     $node->field_plans['und'][0]['node']->field_field_50_59_plan['und'][0]['value'],
          'field_50_59_price' =>     $node->field_plans['und'][0]['node']->field_field_50_59_price['und'][0]['value'],
          'field_50_59_id' =>     $node->field_plans['und'][0]['node']->field_field_50_59_id['und'][0]['value'],
          'field_60_64_plan' =>     $node->field_plans['und'][0]['node']->field_field_60_64_plan['und'][0]['value'],
          'field_60_64_price' =>     $node->field_plans['und'][0]['node']->field_field_60_64_price['und'][0]['value'],
          'field_60_64_id' =>     $node->field_plans['und'][0]['node']->field_field_60_64_id['und'][0]['value'],
      ];

  }
}

function bare_preprocess_user_login(&$vars){
    $theme_path = drupal_get_path('theme', 'bare');
    drupal_add_css($theme_path . '/css/rxbert.css', array('type'=>'file', 'group'=>CSS_THEME, 'weight'=>10));
}
function bare_preprocess_user_pass(&$vars){
    $theme_path = drupal_get_path('theme', 'bare');
    drupal_add_css($theme_path . '/css/rxbert.css', array('type'=>'file', 'group'=>CSS_THEME, 'weight'=>10));
}
/**
 * Additional page variables
 */
function bare_preprocess_page(&$vars) {

  if (!empty($_GET['aaa_agent'])) {
    $agent_id = $_GET['aaa_agent'];
  }

  if (!empty($_GET['agent'])) {
    $agent_id_success = $_GET['agent'];
  }

  $vid = taxonomy_vocabulary_machine_name_load("gtm_codes")->vid;
  $terms = taxonomy_get_tree($vid);

  foreach($terms as $term => $name){
    if($name->name === $agent_id){
        drupal_add_js($name->description, 'inline');
    }

    if($name->name === $agent_id_success){
      drupal_add_js($name->description, 'inline');
    }
  }


  // $taxonomy_term = taxonomy_term_load(1);
  // dsm($taxonomy_term);
  // var_dump($taxonomy_term);
  //die();


  // Check if is first setup of Bare.
  if (variable_get('theme_bare_first_install', TRUE)) {
    include_once('theme-settings.php');
    _bare_install();
  }

  // primary links markup
  if (theme_get_setting('menu_type') == 2) { // use mega menu
    $vars['mainmenu'] = theme('mega_menu', array('menu' => menu_tree_all_data(theme_get_setting('menu_element'))));
  }
  else {
    $vars['mainmenu'] = '';
  }
}

function bare_preprocess_html(&$variables) {
   // Add font awesome cdn.
    drupal_add_css('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css', array(
        'type' => 'external'
      ));

      drupal_add_css('//fonts.googleapis.com/css?family=Lato:300,400,700,900|Droid+Sans:400,700|Fira+Sans+Condensed:300,400,600,700|Open+Sans:300,400,600,700|Great+Vibes', array(
        'type' => 'external'
      ));


}



/**
 * Additional divs around certain forms
 */
function bare_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'comment_node_article_form') {
    $form['#prefix'] = '<div class="content__comment row"><div class="twelve columns special"><ul>';
    $form['#suffix'] = '</ul></div></div>';
  }

  if($form['#form_id'] == 'user_login') {
    unset($form['name']['#description']);
    unset($form['pass']['#description']);
  }
}

/**
 * Additional variables to all labels built into every form
 */

function bare_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // Bootstrap wants us to add a class to the label as well.
  if ($element['#type'] == 'checkbox') {
    $attributes['class'] = 'checkbox';
  }

  // The leading whitespace helps visually separate fields from inline labels.
  if (!empty($variables['rendered_element'])) {
    return ' <label' . drupal_attributes($attributes) . '>' . $variables['rendered_element'] . $t('!title', array('!title' => $title, '!required' => $required)) . "</label>\n";
  }
  else {
    return ' <label' . drupal_attributes($attributes) . '>' . $t('!title', array('!title' => $title, '!required' => $required)) . "</label>\n";
  }
}

/**
 * Setup the HTML markup for all textareas
 */
function bare_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));

  // Add not only the stardard form-text class but the input class so Gumby can relate it better
  _form_set_class($element, array('form-textarea', 'input', 'textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';

  return $output;
}

/**
 * Setup the HTML markup for all textfields
 */
function bare_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));

  // Add not only the stardard form-text class but the input class so Gumby can relate it better
  _form_set_class($element, array('form-text', 'input'));

  // If autocomplete is enabled then setup the extra settings
  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}

/**
 * Need to chagne the button HTML markup for Gumby
 */
/*
function bare_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  return '<div class="medium primary btn form-button"><input' . drupal_attributes($element['#attributes']) . ' /></div>';
}
*/

/**
 * Need to setup the label and input field wrappers
 * Start by adding the class field to the wrapper
 */

function bare_form_element($variables) {
  $element = &$variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
    $attributes['class'][] = 'field';
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<li' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  if ($element['#type'] == 'checkbox') {
    $variables['rendered_element'] = ' ' . $prefix . $element['#children'] . $suffix . "\n";
    $output .= theme('form_element_label', $variables);
  }
  else {
    switch ($element['#title_display']) {
      case 'before':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;
      case 'invisible':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;

      case 'after':
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
        $output .= ' ' . theme('form_element_label', $variables) . "\n";
        break;

      case 'none':
      case 'attribute':
        // Output no label and no required marker, only the children.
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;
    }
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</li>\n";

  return $output;
}


?>