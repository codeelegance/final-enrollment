<?php

/**
 * Overrides of theme implementations
 */
function bare_theme() {
  return array(
    'mega_menu' => array(
      'variables' => array('menu' => NULL),
    ),
    'user_login' => array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bare') . '/templates',
    'template' => 'user-login'
    ),
      'user_pass' => array(
          'render element' => 'form',
          'path' => drupal_get_path('theme', 'bare') . '/templates',
          'template' => 'user-pass'
      )
  );
}

/**
 * Mega drop down primary links.
 *
 * @param <array> $menu
 *   Full array of main menu
 *
 * @return string
 *   Html with mega menu to printo into page
 */
function bare_mega_menu($variables) {

  $menu             = $variables['menu'];
  $alt              = theme_get_setting('menu_alt_class');
  $output           = '<ul class="twelve special main-nav columns">'; // open list

  foreach ($menu as $key => $value) {
    if ($value['link']['hidden'] != 1) { // check if the link is hidden
      $id      = 'menu-main-title-' . $value['link']['mlid']; // give an unique id for better styling
      $options = array();

      if (isset($value['link']['options']['attributes']['title'])) {
        $options = array('attributes' => array('title' => $value['link']['options']['attributes']['title']));
      }
    
      $output .= '<li class="main-nav__link" id="' . $id . '">' . l($value['link']['link_title'], $value['link']['href'], $options);

      if (count($value['below']) > 0 ) { // check if we have children
        $output .= '<div class="dropdown">';
        $output .= '<ul>';

        foreach ($value['below'] as $key2 => $value2) {

          if ($value2['link']['hidden'] != 1) { // check if the link is hidden
            $output .= '<li class="dropdown__link" id="' . $id . '">' . l($value2['link']['link_title'], $value2['link']['href'], $options);
          }

          $output .= '</li>';

        } // end of second foreach loop

        $output .= '</ul>'; // End list
        $output .= '</div>';
      } // end of main check of if dropdown exists inside current item
    } // end check if main link is hidden or not
  } // end foreach loop
      
  return $output;
}