<?php
global $base_url;
?>

<!--Head of page-->

<header style="background-image:url('<?php echo $base_url; ?>/sites/all/themes/custom/bare/img/login.jpg');">
<div class="tint">
		<div class="top-header row">
				<div class="logo-container centered twelve columns">
                    <h1 style="font-weight:900; color:#fefefe; font-size:4rem; line-height:4rem; padding-bottom:1rem; margin:0; text-shadow:rgba(0,0,0.2) 1px 2px 2px;"><?php print $node->field_display_title['und'][0]['value'];?></h1>
				</div>

		</div>

		<div class="row">

		</div>
		</div>
</header>



<!--end head-->

<div class="wrapper">

<!--upgrade-general-info-payment, left column-->
	<div class="row top">
			<section class="seven columns form">

			    <?php if ($_SESSION['return_error']){ ?>
						<div class="danger label" style="background-color:#ff0000; padding:1rem; color:#ffffff; text-align:center;">
						<?php print $_SESSION['return_error'];?>
						<?php unset($_SESSION['return_error']);?>
						</div>
			   <?php }?>


				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
					print render($block['content']);
					print render($content['body']);
                    //print $node->field_cart_info['und'][0]['value'];
				?>
			</section>

	<!--Summary Cart - Right Col-->
				<?php print $node->field_cart_info['und'][0]['value'];?>
	</div>
	<!---end right col-->
	<style>
	.lower {

background: rgba(250,250,250,1);
background: -moz-linear-gradient(top, rgba(250,250,250,1) 0%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(250,250,250,1)), color-stop(51%, rgba(255,255,255,1)), color-stop(100%, rgba(255,255,255,1)));
background: -webkit-linear-gradient(top, rgba(250,250,250,1) 0%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 100%);
background: -o-linear-gradient(top, rgba(250,250,250,1) 0%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 100%);
background: -ms-linear-gradient(top, rgba(250,250,250,1) 0%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 100%);
background: linear-gradient(to bottom, rgba(250,250,250,1) 0%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#ffffff', GradientType=0 );
	}
	</style>
	<!--start of disclamer info-->
	<div class="lower" style="border-top:#eaeaea solid thin; padding-top:2rem; margin-top:4rem;">
		<div class="row" style="margin-bottom:0;">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
		print $disclaimer->body['und'][0]['value'];

        foreach($tiered_plans as $item => $value){
            print "<input type='hidden' data-key='".$item."' value='".$value."'>";
        }

        ?>
		</div>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php
		//$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
		//print $disclaimer->body['und'][0]['value'];

        foreach($tiered_plans as $item => $value){
          print "<input type='hidden' data-key='".$item."' value='".$value."'>";
        }
		?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</footer>
</div>


</div>
<div id="preloader">
    <div class="background-dots">
        <div id="status">
            <div style="width:90%; margin:0 auto;">
                <h3 style="color:#000">Processing Enrollment</h3>
                <img src="https://enroll.americanadvantagesavings.com/sites/all/themes/custom/bare/img/Preloader_7.gif">
            </div>
        </div>
    </div>
</div>

<div id="cover">
    <div id="agent-modal">
        <form id="agentvalidate">
            <h3>Enter Your Representative ID to Continue</h3>
            <div class="form-item webform-component webform-component-textfield">
                <input type="text" class="agentid" name="agentid" value="<?php print $session_agentid; ?>">
            </div>
			<input class="agentvalidatesubmit" type="submit" value="Validate">
			<img class="loading" src="/sites/all/themes/custom/bare/img/pre.gif">
        </form>
        </form>
	</div>

	<div class="error-message">
		<h3>Looks like there was a problem.</h3>
		<p class="error-text"></p>
		<p>Please try again. If the problem persists contact customer service at 888-424-4186</p>
	</div>
</div>
<!--end footer-->