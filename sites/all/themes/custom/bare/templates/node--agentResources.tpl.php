<script type="text/javascript" src="sites/all/themes/custom/bare/js/slick.js"></script>
<link rel="stylesheet" type="text/css" href="sites/all/themes/custom/bare/css/slick.css">
<link rel="stylesheet" type="text/css" href="sites/all/themes/custom/bare/css/slick-theme.css">
<style>
  img.thumb {
    box-shadow:rgba(0,0,0,0.1) 0 0 2px 2px;
  }
  .pad {
    padding:1rem 2rem;
  }
</style>
<header style="background-image:url('sites/all/themes/custom/bare/img/login.jpg');">
<div class="tint agent">
    <div class="top-header row">
        <div class="two columns">

                          <?php
                          if(isset($_SESSION['brand_logo_uri'])){
                            $brand_logo_image = image_style_url("large", $_SESSION['brand_logo_uri']);
                            ?>
                              <img width="300px" src="<?php echo $brand_logo_image; ?>"/>
                          <?php }else{ ?>
                              <img class="logo" src="sites/all/themes/custom/bare/img/truscript-logo2.png"/>
                          <?php } ?>

        </div>

        <div class="eight columns text-right">
        <button id="goBack" onclick="window.history.back()">Back</button>
        </div>

    </div>
    </div>
</header>


<div class="row">
    <div class="centered ten columns">
        <h1 class="text-center">Truscript Agent Resources</h1>
        <p class="text-center subText">A Real Prescription Drug Solution for the Supplemental Health Market.</p>
    </div>
</div>


<div class="row">
  <div class="ten centered columns">


          <div class="row" style="padding-bottom:2rem;">
              <div class="six columns text-center pad">
                  <h4 class="text-center">Truscript Brochure</h4>
                 <img class="thumb" style="border:#eaeaea solid thin;" src="http://mymemberaccess.com/newsletter/truscript/truscript_flyer_2019_agent.jpg" />
                     <div class="primary btn medium">
                       <a href="http://mymemberaccess.com/newsletter/truscript/truscript_flyer_2019_agent.pdf" target="_new">Download Brochure</a>
                      </div>
              </div>

              <div class="six columns text-center pad">
                  <h4 class="text-center">Truscript Sales Guide</h4>
                 <img class="thumb" style="border:#eaeaea solid thin;" src="http://mymemberaccess.com/newsletter/truscript/truscript_sales_guide_2019.jpg" />

                  <div class="secondary btn medium">
                      <a href="http://mymemberaccess.com/newsletter/truscript/truscript_sales_guide_2019.pdf" target="_new">Truscript Sales Guide</a>
                  </div>

              </div>
          </div>


           <div class="row">
              <div class="six columns text-center pad">
                  <h4 class="text-center">Truscript Sales Webinar</h4>
                 <img class="thumb" style="border:#eaeaea solid thin;" src="http://mymemberaccess.com/newsletter/truscript/truscript-webinar.png" />
                     <div class="warning btn medium">
                         <a href="http://mymemberaccess.com/newsletter/truscript/truscript-sales-webinar.mp4" target="_new">Truscript Sales Webinar</a>
                       </div>

              </div>

              <div class="six columns text-center pad">
                  <h4 class="text-center">Truscript Drug Search</h4>
                 <img class="thumb" style="border:#eaeaea solid thin;" src="http://mymemberaccess.com/newsletter/truscript/truscript-landing.png" />


          <div class="info btn medium">
              <a href="http://truscript.com/rx" target="_new">Drug Search Now</a>
          </div>
              </div>
          </div>

  </div>
</div>



<script type="text/javascript">
  (function($){
    $('.agents').slick({
    dots: true,
    infinite: true,
    speed: 1500,
    autoplay:true,
    autoplaySpeed: 8000,
    slidesToShow: 1,
    slidesToScroll: 1
  });


}(jQuery));

</script>