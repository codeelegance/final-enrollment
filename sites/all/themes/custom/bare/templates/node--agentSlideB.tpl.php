<script type="text/javascript" src="sites/all/themes/custom/bare/js/slick.js"></script>
<link rel="stylesheet" type="text/css" href="sites/all/themes/custom/bare/css/slick.css">
<link rel="stylesheet" type="text/css" href="sites/all/themes/custom/bare/css/slick-theme.css">

<header style="background-image:url('sites/all/themes/custom/bare/img/login.jpg');">
<div class="tint agent">
    <div class="top-header row">
        <div class="two columns">
                          <?php
                          if(isset($_SESSION['brand_logo_uri'])){
                            $brand_logo_image = image_style_url("large", $_SESSION['brand_logo_uri']);
                            ?>
                              <img width="300px" src="<?php echo $brand_logo_image; ?>"/>
                          <?php }else{ ?>
                              <img class="logo" src="sites/all/themes/custom/bare/img/truscript-logo2.png"/>
                          <?php } ?>

        </div>

        <div class="eight columns text-right">
        <button id="goBack" onclick="window.history.back()">Back</button>
        </div>
        
    </div>
    </div>
</header>


<div class="row">
    <div class="centered ten columns">
        <h1 class="text-center">Agent Registration Guide</h1>
        <p class="text-center subText">A Real Prescription Drug Solution for the Supplemental Health Market.</p>
    </div>  
</div>


<div class="row">
  <div class="ten centered columns">
      <div class="agents">
        <div class="text-center"><img src="sites/all/themes/custom/bare/img/slider-images/agentReg2.jpeg"/></div>
        <div class="text-center"><img src="sites/all/themes/custom/bare/img/slider-images/agentReg3.jpeg"/></div>
        <div class="text-center"><img src="sites/all/themes/custom/bare/img/slider-images/agentReg4.jpeg"/></div>
        <div class="text-center"><img src="sites/all/themes/custom/bare/img/slider-images/agentReg5.jpeg"/></div>
              <div class="text-center"><img src="sites/all/themes/custom/bare/img/slider-images/agentReg6.jpeg"/></div>

      </div>
</div>
</div>

<script type="text/javascript">
  (function($){
    $('.agents').slick({   
    dots: true,
    infinite: true,
    speed: 1500,
    autoplay:true,
    autoplaySpeed: 8000,
    slidesToShow: 1,
    slidesToScroll: 1
  });


}(jQuery));

</script>