<?php
global $base_url;
?>
<script src="sites/all/themes/custom/bare/js/jsencrypt.js"></script>
<!--Head of page-->
<header class='purple-bkg'>
	<div class="row">
		<div class="six columns afilliate-logo_wrapper">
		<?php
                  if(isset($node->field_brand_logo['und'][0]['uri'])){
                    $brand_logo_image = image_style_url("large", $node->field_brand_logo['und'][0]['uri']);
                    ?>
                      <img width="150px" src="<?php echo $brand_logo_image; ?>"/>
                  <?php }else{ ?>
                      <img src="<?php echo $base_url; ?>/sites/all/themes/custom/bare/img/truscript_logo.png"/>
                  <?php } ?>
		</div>
		<div class="six columns text-right nbfsa-logo_wrapper">
			<img class="nbfsa_logo" src="/sites/all/themes/custom/bare/img/nbfsa-logo-white.png" />
		</div>
	</div>
</header>
<!--end head-->
<div class="decisely">
	<div class="row">
		<div class="large-centered twelve columns">
			<div class="row">
				<h2 class="page-title">Payment Information</h2>
				<div class="eight columns">


						<div class="box-wrapper">

							<div class="box-bar">
								<p>Information Review</p>
							</div>
						<div class="box">
							<div class="heading-section">
								<h1 class="text-center section-heading">Thank You for Providing Your Information</h1>
								<p class="text-center">Please: Take a moment to review your submitted information before proceeding to the billing section below.</p>
							</div>

							<div class="border-top basic-info box-section">
									<h1 class="section-heading">Basic Information</h1>

									<ul class="section-wrap">
										<li class="section info-row1">
											<ul>
												<li>
													<ul>
														<li>
															<span class="label bold">First Name</span>
															<span class="fname"></span>
														</li>

														<li>
															<span class="label bold">SSN</span>
															<span class="ss">*****7866</span>
														</li>

														<li>
															<span class="label bold">Address</span>
															<span class="address">1211 Jamestown Square</span>
														</li>

														<li>
															<span class="label bold">Email</span>
															<span class="email">jane@domainname.com</span>
														</li>

													</ul>







												</li>
											</ul>
										</li>

										<li class="section info-row2">
										    <ul>
												<li>
													<ul>
														<li>
															<span class="label bold">Last Name</span>
															<span class="lname"></span>
														</li>

														<li>
															<span class="label bold">Business Name</span>
															<span class="business_name"></span>
														</li>

														<li>
															<span class="label bold">City</span>
															<span class="city"></span>
														</li>

														<li>
															<span class="label bold">Phone</span>
															<span class="phone"></span>
														</li>

													</ul>

												</li>
											 </ul>
										</li>

										<li class="section info-row3">
											<ul>
												<li>
													<ul>
														<li>
															<span class="label bold">DOB</span>
															<span class="dob"></span>
														</li>

														<li>
															<span class="label bold">Business EIN</span>
															<span class="business_ein"></span>

														</li>

														<li>
															<span class="label bold">State</span>
															<span class="state"></span>
														</li>
													</ul>

												</li>
											 </ul>
										</li>

										<li class="section info-row4">
											<ul>
												<li>
													<ul>
														<li>
															<span class="label bold">Gender</span>
															<span class="gender"></span>
														</li>

														<li>
															<span class="label bold">&nbsp;   </span>
															<span class="spacer"> &nbsp;  </span>
														</li>

														<li>
															<span class="label bold">zip</span>
															<span class="zip"></span>
														</li>
													</ul>

												</li>
											 </ul>
										</li>

									</ul>
								</div>


							<div class="border-top dep-spouse-review box-section">
									<h1 class="section-heading">Dependant / Spouse Information</h1>
											<ul class="section-wrap-dep-review">
												<li class="dependant1">
													<ul class="dep-info-wrap">
															<li class="dep-info">
																<span class="label bold">Relationship</span>
																<span class="rel1"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">First Name</span>
																<span class="dep_fname1"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">Last Name</span>
																<span class="dep_lname1"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">Gender</span>
																<span class="dep_gender1"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">DOB</span>
																<span class="dep_dob1"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">SSN</span>
																<span class="dep_ssn1"></span>
															</li>
													</ul>
												</li>


												<li class="dependant2">
												<ul class="dep-info-wrap">
															<li class="dep-info">
																<span class="label bold">Relationship</span>
																<span class="rel2"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">First Name</span>
																<span class="dep_fname2"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">Last Name</span>
																<span class="dep_lname2"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">Gender</span>
																<span class="dep_gender2"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">DOB</span>
																<span class="dep_dob2"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">SSN</span>
																<span class="dep_ssn2"></span>
															</li>
													</ul>
												</li>


												<li class="dependant3">
												  <ul class="dep-info-wrap">
															<li class="dep-info">
																<span class="label bold">Relationship</span>
																<span class="rel3"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">First Name</span>
																<span class="dep_fname3"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">Last Name</span>
																<span class="dep_lname3"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">Gender</span>
																<span class="dep_gender3"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">DOB</span>
																<span class="dep_dob3"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">SSN</span>
																<span class="dep_ssn3"></span>
															</li>
													</ul>
				 							    </li>

												 <li class="dependant4">
												  <ul class="dep-info-wrap">
															<li class="dep-info">
																<span class="label bold">Relationship</span>
																<span class="rel4"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">First Name</span>
																<span class="dep_fname4"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">Last Name</span>
																<span class="dep_lname4"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">Gender</span>
																<span class="dep_gender4"></span>
															</li>


															<li class="dep-info">
																<span class="label bold">DOB</span>
																<span class="dep_dob4"></span>
															</li>

															<li class="dep-info">
																<span class="label bold">SSN</span>
																<span class="dep_ssn4"></span>
															</li>
													</ul>
				 							    </li>

				  						</ul>

									</div>


									<div class="border-top plan-info box-section">
												<h1 class="section-heading">Plan Information</h1>
												<ul class="section-wrap plan-info-wrap">
													<li class="plan-info_text">
														<span class="label bold">Plan Name</span><br/>
														<span class="plan_name">GAR 2500 Plan</span>
													</li>

													<li class="plan-info_text">
														<span class="label bold">Plan Type</span><br/>
														<span class="plan_type">Family</span>
													</li>

													<li class="plan-info_text">
														<span class="label bold">Plan Rate</span><br/>
														<span class="plan_rate">$453.00/Monthly</span>
													</li>
												</ul>
									</div>

									<div class="border-top box-footer box-section text-center">
										<div class="row">
											<div class="centered eight columns text-center">
												<p class="text-center">If you see any errors in the information above, please go back and correct them before proceeding.</p>
												<a href="#">Go Back Now</a>
											</div>
										</div>
									</div>

						</div>
				</div>
			</div>

						<div class="four columns">
							<div class="box-wrapper">
								<div class="box doc-review">
									<h1 class="section-heading">Document Review</h2>
									<ul class="pdf-review">
										<li>
											<p class="bold">Medical reimbursement Contract</p>
											<a class="medical_contract" href="#" target="_new">View Agreement</a>
										</li>
										<li>
											<p class="bold">Plan Document</p>
											<a class="plan_document" href="#" target="_new">View Plan</a>
										</li>
										<li>
											<p class="bold">Joinder Agreement</p>
											<a class="joinder_agreement" href="#" target="_new">View Agreement</a>
										</li>
									</ul>
								</div>
						</div>
					</div>


			</div>
		</div>
	</div>
</div>

			<div class="row">
				<div class="twelve columns">
					<div class="box-wrapper">
							<div class="box-bar">
								<p>Bank Accounting and Routing Information</p>
							</div>

							<div class="box">
								<div class="heading-section">
									<h1 class="text-center section-heading">To Complete The Process Please Enter Your Banking Information Below</h1>
									<p class="text-center">Please Note: Premiums are paid via automatic bank drafts. The program does not accept credit cards</p>
								</div>

							<div class="row">

								<section class="centered twelve columns form">
									<?php
										$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
										print render($block['content']);
									?>
								</section>
							</div>
							</div>
					</div>
				</div>
			</div>


		<div id="preloader">
				<div id="status">
							<div style="width:90%; margin:0 auto;">
								<h3 style="color:#000">Processing Enrollment</h3>
								<img src="/sites/all/themes/custom/bare/img/Preloader_7.gif">
							</div>
				</div>
    		</div>





<br/><br/><br/><br/>
<!--end footer-->