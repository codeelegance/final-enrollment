<?php
//dsm($node);
?>
<span style='display:none;' id='nid' nid='<?php print $node->nid ?>'></span>
<div class="head">
	<div class="row header_box">
	  <header class="twelve coloumns">
	  	<div class="eight columns title">
				<h1><?php print $node->title ?></h1>
			</div>	
<!--Summary Cart - Right Col-->
			<section class="four columns summary-box">
				<div class="yep-i-am-awesome">
					<ul class="two_up tiles summary-box--container left">
								<li class="summary-box--heading">Membership Program<p class="summary-box--result">Usap Health Card</p></li>
								<li class="summary-box--heading">Registration Fees<p class="summary-box--result" id="summary-box--reg">$0.00</p></li>
								<li class="summary-box--heading">Membership Fees<p class="summary-box--result" id="summary-box--mem">$0.00</p></li>
								<li class="summary-box--heading">Billing Cycle<p class="summary-box--result">monthly</p></li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<p class="price-total">Total Fees<span class="total-price"> $0.00</span></p>
			</section>
		</header>
	</div>
</div>
<!--end head-->
<div class="wrapper">

<!--upgrade-general-info-payment, left column-->

	<div class="row top">
		<section class="twelve columns form">
		<?php
			$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
			print render($block['content']);
		?>
		</section>
	<!---end info-box-->
	<!--start of disclamer info-->
	<div class="row lower">
		<?php print ($node->field_disclaimer['und'][0]['value']) ? $node->field_disclaimer['und'][0]['value'] : '' ?>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
 		<p class="copyright">USAP  20113</p>    
 		<p class="terms"><a href="#"?>Terms of Service</a></p>
 		<p class="privacy"><a href="#">Privacy Policy</a></p>
	</footer>
</div>
<!--end footer-->

</div>
