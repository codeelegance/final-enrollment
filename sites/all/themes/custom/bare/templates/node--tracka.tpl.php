<?php

?>
<!--Head of page-->
<div class="head">
	<div class="row header_box">
	  <header class="twelve coloumns">
	  	<div class="eight columns title">
				<h1><?php print $node->field_display_title['und'][0]['value']; ?></h1>
			</div>
<!--Summary Cart - Right Col-->
				<?php print $node->field_cart_info['und'][0]['value'];?>
		</header>
	</div>
</div>
<!--end head-->

<div class="wrapper">
	<div class="row section-hightlight">
		<ul class="progressbar">
			<li rel="upgrades" class="active info-title"><span class="info-title">Select Your Coverage</span></li>
			<li rel="create-account" class="inactive info-title"><span class="info-title">Create Your Account</span></li>
			<li rel="payment" class="inactive info-title"><span class="info-title">Payment Info</span></li>
		</ul>
	</div>
	<div class="row top">
		<section class="twelve columns form">
			<section class="info-box" id="upgrade">
				<div class="info-box__content upgrades">
					<div class="six columns left-content">
						<h2>Select Your Additional Coverage</h2>
						<p>Additional insurance coverage is provided at a monthly rate of 67 &cent; per $10,000 of coverage. <a href='http://usadvantagesavings.com/misc/VoluntaryInsuranceSummary.pdf'>Summary of Voluntary Coverage</a></p>
						<p>Insurance is provided on a guaranteed issue basis. No one can be denied coverage.
						</p>
						<p class="no-thanks"><a id='free-link' class="sub-link" href="#">No Thanks, I'll take the free 3,000 in coverage</a></p>
					</div>

					<div class="six columns coverage-select">
						
						<ul class="cs-style-1" id="plans">
						</ul>
					</div>
				</div>
			</section>
				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
					print render($block['content']);
				?>
<div class="clearfix"></div>
		<p class="left-footer-msg">Read the formal descriptions of the discount <a href="http://www.usadvantagesavings.com/health/details/">medical benefits.</a></p>
		<p class="right-footer-msg">USAdvantagePlans are defined memberships in the <a href="http://usadvantagesavings.com/health/aaa">American Advantage Association.</a></p>
	</div>	
<div class="clearfix"></div>
	<!--start of disclamer info-->
	<div class="row lower">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
		print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
		print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</footer>
</div>
<!--end footer-->

