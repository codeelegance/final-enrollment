<?php

?>
<!--Head of page-->
<div class="head2">
	<div class="row">
	  <header class="twelve coloumns">
		<h1><?php print $node->field_display_title['und'][0]['value']; ?></h1>
		<?php print ($node->field_subhead['und'][0]['value']) ? "<h2>" . $node->field_subhead['und'][0]['value'] . "</h2>" : '' ?>
		</header>
	</div>
</div>
<!--end head-->

<div class="free-stuff">
	<div class="row row-free">
		<div class="twelve columns">
			<ul class="free">
				  <li id="free-gift"> 
				  	<img src="../sites/all/themes/custom/bare/img/check-green.png"/>
						<span>Almost There! Fill Out The Application Below. </span>
					</li>

					<li id="free-gift-upgrade">
						<img src="../sites/all/themes/custom/bare/img/check.png"/>
						<span>Great, You've selected your plan.</span>
					</li>
				</ul>
</div></div>
				</div>

<div class="wrapper">
<div class="row">

				
</div>
</div>
<!--upgrade-general-info-payment, left column-->
	<div class="row top">

			<section class="seven columns form">
				
				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
					print render($block['content']);
				?>
			</section>

	<!--Summary Cart - Right Col-->
				<?php print $node->field_cart_info['und'][0]['value'];?>
				
	</div>

	<!---end right col-->

	<!--start of disclamer info-->
	<div class="row lower">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
		print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php
		//$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
		//print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</footer>
</div>
<!--end footer-->

