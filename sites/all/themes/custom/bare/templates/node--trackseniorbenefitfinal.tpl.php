<style>

	.user-box_row {
		position:relative;
	}
	
	.tint{
		padding:0;
	}
	
 	.user-box_wrapper{
		padding:2rem;
		background-color:#ffffff;
		margin: 4rem auto;
	  	position:relative;
	  	box-shadow:rgba(0,0,0,0.1) 1px 1px 2px 2px;
 	}

 	.user-box_wrapper img {
 		max-width:20rem;
 	}

 	.login-fields{
 		text-align:left;
 		padding:0 3rem;
 		margin:2rem 0;
 	}

 	 input.form-text {
 		width:100%;
 		max-width:100%;
 		height:3rem;
 	}

 	 	input.form-text:focus {
 		background-color:#ffffff;
 		border:#27B994 1px solid;
 	}



	#edit-submit--2 {
	    padding:5px 15px; 
	    background:#e54b65; 
	    border:0 none;
	    cursor:pointer;
	    color:#ffffff;
	    font-size:1rem;
	    padding:1rem 2rem;
	    width:100%;
	    margin-top:1rem;
	}

		#edit-submit--2:hover {
	    background:#dd2040; 
	}

	.form{
		margin-left:0;
	}

	.footer {
		background-color:#efefef;
		padding-top:2rem;
		margin-bottom:0;
		padding-bottom:0;
	}

	footer.row {
		margin-bottom:0;
	}

	.webform-client-form .inactive{
		background-color:transparent;
		padding:0;
		margin:0;
		font-size:1.3rem;
	}

	#webform-component-verify-your-enrollment-header{
		margin:0;
		margin-bottom:2rem;
		text-align: center;
	}

	#webform-component-firstlast, #webform-component-firstName {
		text-align:left;
	}

	.verify-copy{
		padding:2rem;
	}

	.white {
		color:#ffffff;
	}

	.header {
		font-weight:700;
		font-size:1.2rem;
	}

	ul {
		margin-left:1rem;
	}

	ul li {
		list-style-type: circle;
		margin-left:1rem;
	}

	#webform-component-firstlast  p {
		font-style:italic;
	}



 
</style>

<script>
	jQuery(document).ready(function( $ ) {
			var windowHeight = $(window).height();

			$('.background-wrapper').css({height: windowHeight});
			$('.form').css({height: windowHeight});

			topCenter = windowHeight / 2;
			var userBox = $('.user-box_wrapper').height()/2;
			var copyHeight = $('.verify-copy').height()/2 ;

			if ( $(window).height() < 685 && $('body').hasClass('page-node-662') || $('body').hasClass('page-node-660') ){
				$('.user-box_wrapper').css({marginTop:0});
				$('.background-wrapper').css({height: '100%'});
				$('.form').css({height: '100%'});
			}else {
				//$('.user-box_wrapper').css({marginTop:topCenter - userBox -50});
			}
			$('.verify-copy').css({marginTop:topCenter - copyHeight - 50});

				$(window).resize(function(){
					var windowHeight = $(window).height();
					$('.background-wrapper').css({height: windowHeight});
					$('.form').css({height: windowHeight});
					topCenter = windowHeight / 2;
					var userBox = $('.user-box_wrapper').height()/2;
					var copyHeight = $('.verify-copy').height()/2 ;

					if ( $(window).height() < 685 && $('body').hasClass('page-node-662') || $('body').hasClass('page-node-660') ){
						$('.user-box_wrapper').css({marginTop:0});
						$('.background-wrapper').css({height: '100%'});
						$('.form').css({height: '100%'});
					}else {
						//$('.user-box_wrapper').css({marginTop:topCenter - userBox -50});
					}	    
						$('.verify-copy').css({marginTop:topCenter - copyHeight - 50});
				});
	 });

</script>

<div class="background-wrapper" style="width:100%; background-size:cover; background-position:center center; background-image:url('sites/all/themes/custom/bare/img/login.jpg');">
	<div class="tint">
		<div class="row">
		<?php print render($block['content']);?>
		<div class="top-header row">
				<section class="eight centered columns form">
		   			<div class="user-box_wrapper ">
			   			<div class="text-center">

                              <img class="logo" src="https://web2.nbfsa.com/secure_enroll/enrollment/sites/default/files/styles/large/public/benefit-savers-logo.png?itok=q5QbOQEj"/>

					 	</div>

					 	 <?php if ($_SESSION['return_error']){ ?>
							<div class="danger label" style="background-color:#ff0000; padding:1rem; color:#ffffff; text-align:center;">
							<?php print $_SESSION['return_error'];?>
							<?php unset($_SESSION['return_error']);?>
							</div>
			   			<?php }?>


					 	<?php
							if($nid == '2253'){  //enroll-final
									?>
									<form class="enroll-final" action="process-verify2" method="POST">
									<?php
			                		print enrollment_parse_template($content['body']['#items'][0]['value'],$_GET);
			                		?>
			                		</form>
			                		<?php
                            }elseif($nid == '2252'){ //enroll-verify
                              print enrollment_parse_template($content['body']['#items'][0]['value'], ['client_enrollment_code' => $_GET['u']]);
                            }else{
                              print render($content['body']);
                            }
			                ?>
					</div>
				</section>
		</div>
	</div>
</div>
