<div id="block-<?php print $block->module . '-' . $block->delta; ?>" class="block--<?php print $block->delta; ?>" <?php print $attributes; ?>>
	<?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
	  <h2><?php print $block->subject ?></h2>
	<?php endif;?>
	<?php print render($title_suffix); ?>

	<div class="block--<?php print $block->delta; ?>__content"<?php print $content_attributes; ?>>
		<?php print $content ?>
	</div>
</div>