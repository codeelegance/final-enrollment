<?php

//gotta grab a field from this nid on redirect from the enrollment page
$params = drupal_get_query_parameters();
$agent = explode('/', $params['destination']);
$agent = node_load($agent[1]);

?>
<style>

	.user-box_row {
		position:relative;
	}

	
 	.user-box_wrapper{
		padding:3rem;
		background-color:#ffffff;
		margin: auto;
	  	position:relative;
	  	box-shadow: rgba(0,0,0,0.1) 1px 1px 1px 1px
 	}

 	.user-box_wrapper img {
 		max-width:20rem;
 	}

 	.login-fields{
 		text-align:left;
 		padding:0 3rem;
 		margin:2rem 0;
 	}

 	.login-fields input[type='text'],.login-fields input[type='password']  {
 		width:100%;
 		max-width:100%;
 		height:3rem;
 	}

 	 	.login-fields input[type='text']:focus,.login-fields input[type='password']:focus  {
 		background-color:#ffffff;
 		border:#27B994 1px solid;
 	}



	input[type=submit] {
	    padding:5px 15px; 
	    background:#e54b65; 
	    border:0 none;
	    cursor:pointer;
	    color:#ffffff;
	    font-size:1rem;
	    padding:1rem 2rem;
	    width:100%;
	    margin-top:1rem;
	}

 
</style>

<script>
	jQuery(document).ready(function( $ ) {
			var windowHeight = $(window).height();
			$('.background-wrapper').css({height: windowHeight});

			topCenter = windowHeight / 2;
			userBox = $('.user-box_wrapper').height();

			$('.user-box_wrapper').css({marginTop:topCenter - 300});

			$(window).resize(function(){
				var windowHeight = $(window).height();
				$('.background-wrapper').css({height: windowHeight});

				topCenter = windowHeight / 2;
				userBox = $('.user-box_wrapper').height();

				$('.user-box_wrapper').css({marginTop:topCenter - 300});

			});
	 });

</script>

<div class="background-wrapper is this the right template" style="width:100%; background-size:cover; background-position:center center; background-image:url('../sites/all/themes/custom/bare/img/login.jpg');">
	<div class="tint">
		<div class="wrapper">
			<div class="row top">
					<section class="six centered columns form">
						
					    <?php if ($_SESSION['return_error']){ ?>
								<div class="danger label" style="background-color:#ff0000; padding:1rem; color:#ffffff; text-align:center;">
								<?php print $_SESSION['return_error'];?>
								<?php unset($_SESSION['return_error']);?>
								</div>
					   <?php }?>
					   	<div class="row user-box_row">
					   		<div class="user-box_wrapper text-center">
                                <?php //var_dump($_SESSION['brand_logo_uri']);
                                if(isset($agent->field_brand_logo['und'][0]['uri'])) {
                                  $brand_logo_image = image_style_url("large", $agent->field_brand_logo['und'][0]['uri']);
                                }else{
                                  $brand_logo_image = "../sites/all/themes/custom/bare/img/truscript-logo2.png";
                                }
                                ?>
						   				<img class="logo" src="<?php print $brand_logo_image; ?>"/>
										<h5><?php print $agent->field_login_subhead['und'][0]['value'] ?></h5>
										<div class="login-fields">
										<?php
										 print drupal_render($form['name']);
								   		 print drupal_render($form['pass']);
								   		 print drupal_render($form['actions']);
								   		 print drupal_render($form['form_build_id']);
							    		print drupal_render($form['form_id']);
								   		 ?>
								   		 </div>
					   		</div>
					   	</div>
					</section>

			
			</div>
			<!---end right col-->
		</div>
	</div>
</div> <!-- End of background wrapper -->