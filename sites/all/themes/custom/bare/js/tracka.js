(function($) {
  Drupal.behaviors.enrollment = {
    attach: function (context, settings) {
      var basePath = Drupal.settings.basePath;
      var nid = Drupal.settings.enrollmentNid;
      $('.fieldset-wrapper').hide();
      $("input[name='submitted[path_nid]']").val(nid);
      $("input[name='submitted[enrollment_path]']").val($(location).attr('pathname'));
      $("#webform-component-payment--payment-col-1--payment-type > label").hide();
      $("#webform-component-payment--payment-col-1--credit-card--card-type > label").hide();
      $('#webform-component-personal-info--info-col-2--birth-date select').addClass('required');

      //Ability to "back up" to previous form sections
      $(".progressbar li").click(function(){
        if ($(this).hasClass('inactive') && $(this).hasClass('processed')) {
          $(this).removeClass('processed');
          $(".webform-component-fieldset > .fieldset-wrapper").hide();
					$('#edit-actions--2').hide();
					$('section#upgrade').hide();
					switch ($(this).attr('rel')) {
						case "upgrades":
							$('section#upgrade').slideDown();
							$('#webform-component-personal-info--agreement-fieldset--next').show();
							$(".progressbar li[rel='payment'] span").show();
						break;
						case "create-account":
							$("#webform-component-personal-info .fieldset-wrapper").slideDown();
						break;
					}
          $(this).siblings("li").removeClass('active').addClass('inactive');
          $(this).removeClass('inactive').addClass('active');
        }
      });   

      $('#plans').load(basePath + 'ajax/update-plans', {path:nid}, function() {
        $('li.coverage-option a.add').click(function(){
          var plan = $(this).attr('nid');
          var price = $(this).attr('price');
          var pathname = $(location).attr('pathname');
          pathname_array = pathname.split('/');
          path = pathname_array[3];
          

          //Move to the next step
          $('#summary-box').fadeOut().fadeIn();
          $('#upgrade').slideUp();
          $(".progressbar li[rel='upgrades']").removeClass('active').addClass('inactive processed');
          $('#webform-component-personal-info .fieldset-wrapper').slideDown();
          $(".progressbar li[rel='create-account']").removeClass('inactive').addClass('active');
          $('#webform-component-personal-info--agreement-fieldset--next').show();
          $('#webform-component-payment-header').show();
					
         
          //Update the summary box
          $.getJSON(basePath + "ajax/update-summary", {path:path, plan:plan}, function( data ) {
            if (data.reg != 0) {
              var firstMonth = +data.price + +data.reg;
              $('#summary-box--reg').html('$' + parseFloat(data.reg).toFixed(2));
              $('#summary-box-first-total').html('$' + firstMonth);
              $('#summary-box li.registration').show();
              $('#summary-box p.price-reg-total').show();
              $('#sig-price-one').html(firstMonth);
            }
            else {
              $('#sig-price-one').html(parseFloat(data.price).toFixed(2));
              $('#summary-box li.registration').hide();
              $('#summary-box p.price-reg-total').hide();
            }
            if (data.fee != 0) {
              $('#summary-box--mem').html('$' + parseFloat(data.fee).toFixed(2));
              $('#summary-box li.membership').show();
            }
            $('#summary-plan').html(data.package_name);
            $('#summary-box-total').html('$' + parseFloat(data.price).toFixed(2));
 
            //Update the Disclaimer
            $('#sig-price-two').html(parseFloat(data.price).toFixed(2));
            //Update the hidden form elements
            $("input[name='submitted[price]']").val(price);
            $("input[name='submitted[plan_nid]']").val(plan);
           $("input[name='submitted[package]']").val(data.package_id);
          });
        });
				
				$('#free-link').click(function(){
          //Move to the next step
          $('#upgrade').slideUp();
          $(".progressbar li[rel='upgrades']").removeClass('active').addClass('inactive processed');
          $('#webform-component-personal-info .fieldset-wrapper').slideDown();
          $(".progressbar li[rel='create-account']").removeClass('inactive').addClass('active');
          $('#webform-component-personal-info--agreement-fieldset--next').hide();
          $(".progressbar li[rel='payment'] span").hide();
          $('#summary-box').fadeOut();
          $('#summary-plan').html("USAP Health Card with $3,000 AD&D");
          $('#summary-box ul li.registration').hide();
          $('#summary-box ul li.membership').hide();
          $('#summary-box ul li.premium').hide();
          $('#summary-box .price-reg-total').hide();
          $('#summary-box-total').html("$0.00");
          $('#summary-box').fadeIn();
          $("input[name='submitted[price]']").val(0);
          $("input[name='submitted[plan_nid]']").val(9);
          $("input[name='submitted[package]']").val("CHUBB USAP Health Card with $3,000 AD&D");        
          $('#edit-actions--2').show();

          //No payment, so no required fields
          $('#webform-component-payment--payment-col-1--credit-card .form-text').removeClass('required');
          $('#webform-component-payment--payment-col-1--credit-card .form-select').removeClass('required');
          $('#webform-component-payment--payment-col-1--check .form-text').removeClass('required');
          $('#edit-submitted-payment-payment-col-2-e-sig').removeClass('required');
          return false;
				});
				
			// Adds tracking to the upgrade buttons
		  $(".price-option").each(function(){
			var text = $(this).text();
			var eventTracking = ", 'Button','Upgrade']);"
			$( this ).next().attr("onClick","_gaq.push(['_trackEvent'," + " ' " + text + " ' " + eventTracking );
		  });
		  
      });
			
			
      //Go to payment section
      $('.add-btn-next').click(function(){
        if (check_required()) {
					$('#webform-component-personal-info > .fieldset-wrapper').slideUp();
					$(".progressbar li[rel='create-account']").removeClass('active').addClass('inactive processed');
					$('#webform-component-payment .fieldset-wrapper').slideDown();
					$(".progressbar li[rel='payment']").removeClass('inactive').addClass('active');
					$('#webform-component-personal-info--next').hide();
					$('#webform-component-payment-header').hide();
					$('#summary-plan').html("USAP Health Card with $3,000 AD&D");
					$("input[name='submitted[price]']").val(0);
					$("input[name='submitted[plan_nid]']").val(9);
					$("input[name='submitted[package]']").val("CHUBB USAP Health Card with $3,000 AD&D");        
					$('#edit-actions--2').show();

          //Set "required" fields
          $('#webform-component-payment--payment-col-1--credit-card .form-text').addClass('required');
          $('#webform-component-payment--payment-col-1--credit-card .form-select').addClass('required');          
          $('#edit-submitted-payment-payment-col-2-e-sig').addClass('required');
				}
      });
	  
	  //tracking for the submit button
	  $("#edit-submit--2").attr("onClick","_gaq.push(['_trackEvent', 'Button', 'Enroll', 'Submit Button']);");

      
      $('#edit-submit--2').click(function(){
        if (check_required()) {
          return true;  
        }
        else {
          return false;
        }
      });

      //Credit card or check
      $('#webform-component-payment--payment-col-1--check').hide();
      $('#webform-component-payment--payment-col-1--billing-info').hide();
      $('.form-item-submitted-payment-payment-col-1-payment-type input').change(function(){
        radio = ($(this).attr('id'));
        if (radio == 'edit-submitted-payment-payment-col-1-payment-type-1') {
          $("#webform-component-payment--payment-col-1--check").hide();
          $("#webform-component-payment--payment-col-1--credit-card").show();
          $('#webform-component-payment--payment-col-1--credit-card .form-text').addClass('required');
          $('#webform-component-payment--payment-col-1--credit-card .form-select').addClass('required');
          $('#webform-component-payment--payment-col-1--check .form-text').removeClass('required');
        }
        else if (radio == 'edit-submitted-payment-payment-col-1-payment-type-2') {
          $("#webform-component-payment--payment-col-1--check").show();
          $("#webform-component-payment--payment-col-1--credit-card").hide();
          $('#webform-component-payment--payment-col-1--credit-card .form-text').removeClass('required');
          $('#webform-component-payment--payment-col-1--credit-card .form-select').removeClass('required');
          $('#webform-component-payment--payment-col-1--check .form-text').addClass('required');
        }
      });
      //Different Billing information
      $('#edit-submitted-payment-payment-col-1-billing-different-1').click(function(){
        $('#webform-component-payment--payment-col-1--billing-info').toggle();
      });
    }
  }
  function check_required() {
    var result = true;
    
    //Global reset
    $('.webform-client-form .form-text').attr('style', '');
    $('.webform-client-form .form-select').attr('style', '');
    $('.webform-client-form label.checkbox').attr('style', '');
    
    $(".required").each(function(i){
      if ($(this).val() == '') {
        $(this).css('border', '2px solid red');
        result = false;
      }
    });

    $('#edit-submitted-personal-info-agreement-fieldset-agreement-1').parent('label').removeClass('empty');
    if ($('#edit-submitted-personal-info-agreement-fieldset-agreement-1').attr('checked') != 'checked') {
      $('#edit-submitted-personal-info-agreement-fieldset-agreement-1').parent('label').addClass('empty');
      result = false;
    }

    if ( !isValidEmailAddress( $('#edit-submitted-personal-info-info-col-2-email').val() ) ) {
        $('#edit-submitted-personal-info-info-col-2-email').css('border', '2px solid red');
        result = false;
      };
			
		if ( !isValidZip( $('#edit-submitted-personal-info-info-col-2-zip').val() ) ) {
			$('#edit-submitted-personal-info-info-col-2-zip').css('border', '2px solid red');
			result = false;
		};
		
    return result;
  }
  //check if the zip is a 5 number zip code
  function isValidZip(zip) {
		var pattern = new RegExp(/^\d{5}$/);
		return pattern.test(zip);
    };

  //We need to check the email on the client side or we get a hidden error later
  function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
  }; 
      
})(jQuery);
