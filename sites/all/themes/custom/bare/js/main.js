(function($) {

	var Bare = {}; //global object for namespacing all functions

	$(document).ready(function(){
		// Gumby is ready to go


		Gumby.ready(function() {
			Gumby.log('Gumby is ready to go...', Gumby.dump());

			// placeholder polyfil
			if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
				$('input, textarea').placeholder();
			}

			// skip link and toggle on one element
			// when the skip link completes, trigger the switch
			$('#skip-switch').on('gumby.onComplete', function() {
				$(this).trigger('gumby.trigger');
			});

		// Oldie document loaded
		}).oldie(function() {
			Gumby.warn("This is an oldie browser...");

		// Touch devices loaded
		}).touch(function() {
			Gumby.log("This is a touch enabled device...");
		});

        var code = $("input[name='submitted[import_id]']").val();
        var tree_name = $("input[name='submitted[tree_name]']").val();
		//populate the import id wiht some ajax to affinity

		$.getJSON("/validate-agent/"+code+"/"+tree_name,{response_format: "JSON"},
			function(jsonData) {

				let params = new URLSearchParams(jsonData);

				// (jsonData.replace(/\"/g, ""));
				if (params.get("response_code") == "OK") {

					var import_id = params.get("agent_id");
					callback(import_id);

				} else {

					return false;
				}
			},
			function(xhr, ajaxOptions, thrownError) {
				$('input#promo').css('border', '1px solid #ff0000');
				console.log("There was a problem with your enrollment process. Please Login in Again. If the problem persists contact customer service at 877-403-4919");
				return false;
			}
		);

		
		// Boot up BARE! Happy Coding :)
	  Bare.main.init();

	});


	/******************************************************************************************
	*******************************************************************************************
	MAIN:
	module for providing the main functionality and initializing features for BARE
	*******************************************************************************************
	*******************************************************************************************/
	Bare.main = (function() {

		var init = function() {
			//replace_placeholders();
			//input_errors();
		};
		


		/**
		 * Module for replace all labels with placeholders
		 */
		var replace_placeholders = function() {

			var phForms  = $('form');
			var phFields = 'input[type=text], input[type=email], textarea';

		  phForms.find(phFields).each(function(){ // loop through each field in the specified form(s)

			  var el 			= $(this), // field that is next in line
						wrapper = el.parents('.form-item'), // parent .form-item div
						lbl     = wrapper.find('label'), // label contained in the wrapper
						lblText = lbl.text(); // the label's text

			  // add label text to field's placeholder attribute
			  el.attr("placeholder",lblText);

			  // hide original label
			  lbl.hide();

		  });

		};

		/**
		 * Adds Gumby specific classes to all form errors
		 */
		var input_errors = function() {

			var inputs = $('.input');

			inputs.each(function() {
				if($(this).hasClass('error') && $(this).hasClass('textarea')){
					$(this).removeClass('error');
					$(this).parent().parent('li').addClass("danger");
				}else if($(this).hasClass('error') && !$(this).hasClass('textarea')){
					$(this).removeClass('error');
					$(this).parent('li').addClass("danger");
				}
			});

		};
		
		return {
			init: init	
		};

	})();
/*
$(function() {
$('.towing, .fuel, .jump-start, .lock-outs, .tire-changes').hide();
$('li[rel="two"], li[rel="three"], li[rel="four"], li[rel="five"], li[rel="six"]').hover(function() {
   $('.the-plan').hide();
	 $.find('ul.benefits li[rel="#"]').show();


	 });

	$(function() {
	$('.poopnstuff').hide();


  $('.plan-details').hover(function() {
  $('.poopnstuff').fadeToggle('fast');
  

  });

  });

});

*/
})(jQuery);




