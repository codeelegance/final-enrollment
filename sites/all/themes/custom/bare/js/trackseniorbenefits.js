(function($) {
    Drupal.behaviors.enrollment = {
        attach: function (context, settings) {
            var basePath = Drupal.settings.basePath;
            var nid = Drupal.settings.enrollmentNid;
            //$('.fieldset-wrapper').hide();
            $("input[name='submitted[path_nid]']").val(nid);
            console.log($(location).attr('hostname'));
            console.log(basePath);
            var pathname = $(location).attr('pathname');
            pathname_array = pathname.split('/');
            path = pathname_array[3];

            //grab parameters
            var url = window.location.href;
            if(url.split('?')[1] != undefined) {
                var arguments = url.split('?')[1].split('=');
                arguments.shift();
                agentid = arguments[0];
            }else{
                agentid = null;
            }

            //reconstruct the end of the path
            var newPathname = "";

            if ($(location).attr('hostname') != 'enrollment.localhost') {
                for (i = 3; i < pathname_array.length; i++) {
                    newPathname += "/";
                    newPathname += pathname_array[i];
                }
            } else {
                newPathname = $(location).attr('pathname');
            }
// Test Agent - 133036
            // agent id = RX0284
            $('.danger').hide();
            $('#preloader').hide();
            $('#edit-actions--2').show();

            $("input[name='submitted[enrollment_path]']").val(newPathname);
            $("#webform-component-payment--payment-type > label").hide();
            $("#webform-component-payment--credit-card--card-type > label").hide();
            $('#webform-component-personal-info--info-col-2--birth-date select').addClass('required');
            $('#edit-submitted-electronic-signature-do-you-agree-1 input').addClass('required');



            //assign the right package ID to the package selection
            $('.form-item-submitted-personal-info-package').on('click', function(){
               if($('#edit-submitted-personal-info-package-2').is(':checked')){
                   $('input[name="submitted[enroll_id]"]').val(1194);
               }
                if($('#edit-submitted-personal-info-package-1').is(':checked')){
                    $('input[name="submitted[enroll_id]"]').val(1193);
                }
                if($('#edit-submitted-personal-info-package-3').is(':checked')){
                    $('input[name="submitted[enroll_id]"]').val(1196);
                }
            });

            //price of package for final enrollment
            if(arguments[7] != undefined) {
                var package_url = arguments[7].split('&');
                var package_url = package_url[0];
                console.log(package_url[0]);

                if (package_url == 1193) {
                    $('span.price').html('19.95');
                }
                if (package_url == 1194) {
                    $('span.price').html('24.95');
                }
                if (package_url == 1196) {
                    $('span.price').html('32.95');
                }
            }


            if(agentid != undefined){

                $('#cover').hide();
                $("input[name='submitted[agent_id]']").val(agentid);

            }

            //agent validation
            $('#agentvalidate').submit(function(e){
                e.preventDefault();
                //alert($("input[name='agentid']").val());
                $('.error-message').removeClass('show').fadeOut();
                var code = $("input[name='agentid']").val();
                var tree_name = $("input[name='submitted[tree_name]']").val();

                console.log(code+' '+tree_name);

                var import_id = getImportID(code, tree_name, function(import_id){
                    console.log(import_id);
                    if(import_id !== false) {
                        $('#cover').hide();
                        $("input[name='submitted[agent_id]']").val(import_id);
                        $("input[name='submitted[import_id]']").val(import_id);
                    }
                });


            });

            function getImportID(agentID, tree, callback) {
                $('.loading').show();

                $.getJSON("/validate-agent/"+agentID+"/"+tree,{response_format: "JSON"},
                    function(jsonData) {
                        console.log(jsonData);
                        let params = new URLSearchParams(jsonData);

                        // (jsonData.replace(/\"/g, ""));
                        if (params.get("response_code") == "OK") {

                            var import_id = params.get("agent_id");
                            callback(import_id);
                            $('.loading').hide();

                        } else {
                            //alert(params.get('error_string'));
                            $('.loading').hide();
                            $('.error-message').addClass('show');
                            $('.error-text').text(params.get('error_string'));

                            return false;
                        }
                    },
                    function(xhr, ajaxOptions, thrownError) {
                        $('input#promo').css('border', '1px solid #ff0000');
                        console.log("There was a problem with your enrollment process. Please Login in Again. If the problem persists contact customer service at 877-403-4919");
                        return false;
                    }
                );
            }

            if( $('form').prop('id') == 'webform-client-form-2813') {//HC2U SINGLE - Agent

                $('#webform-component-personal-info--membership-type input:radio').on( 'change', (function(){
                    var value = this.value;
                    //alert(value);
                    if(value == 'MEMBERONLY'){
                        $('input[name="submitted[level]"]').val('MEMBERONLY');
                        $('#summary-box-total').text('$67.50 monthly');
                        $('span.deduct_price').text('$67.50');

                    }

                    if ( value == 'MEMBER+CHILD'){
                        $('input[name="submitted[level]"]').val('MEMBER%2bCHILD');
                        $('#summary-box-total').text('$172.50 monthly');
                        $('span.deduct_price').text('$172.50');

                    }

                    if (value == 'MEMBER+SPOUSE'){
                        $('input[name="submitted[level]"]').val('MEMBER%2bSPOUSE');
                        $('#summary-box-total').text('$120.00 monthly');
                        $('span.deduct_price').text('$120.00');

                    }

                    if ( value == 'FAMILY'){
                        $('input[name="submitted[level]"]').val('FAMILY');
                        $('#summary-box-total').text('$225.00 monthly');
                        $('span.deduct_price').text('$225.00');

                    }

                }));

            }


            if( $('form').prop('id') == 'webform-client-form-2814') {//HC2U PLUS - Agent

                $('#webform-component-personal-info--membership-type input:radio').on( 'change', (function(){
                    var value = this.value;
                    //alert(value);

                    if(value == 'MEMBERONLY'){
                        $('input[name="submitted[level]"]').val('MEMBERONLY');
                        $('#summary-box-total').text('$82.50 monthly');
                        $('span.deduct_price').text('$82.50');

                    }

                    if ( value == 'MEMBER+CHILD'){
                        $('input[name="submitted[level]"]').val('MEMBER%2bCHILD');
                        $('#summary-box-total').text('$187.50 monthly');
                        $('span.deduct_price').text('$187.50');

                    }

                    if (value == 'MEMBER+SPOUSE'){
                        $('input[name="submitted[level]"]').val('MEMBER%2bSPOUSE');
                        $('#summary-box-total').text('$135.00 monthly');
                        $('span.deduct_price').text('$135.00');

                    }

                    if ( value == 'FAMILY'){
                        $('input[name="submitted[level]"]').val('FAMILY');
                        $('#summary-box-total').text('$240.00 monthly');
                        $('span.deduct_price').text('$240.00');

                    }

                }));
            }


            //Ability to "back up" to previous form sections
            $(".webform-component-markup h3").click(function () {
                if ($(this).hasClass('inactive') && $(this).hasClass('processed')) {
                    $(this).removeClass('processed');
                    $(this).parent("div").siblings(".webform-component-fieldset").children(".fieldset-wrapper").slideUp();
                    $(this).parent("div").next(".webform-component-fieldset").children(".fieldset-wrapper").slideDown();
                    $(".webform-component-markup h3").removeClass('active').addClass('inactive');
                    $(this).removeClass('inactive').addClass('active');
                    $('#edit-actions--2').hide();
                }
            });

            //moved this to first for cdp
            $('#webform-component-personal-info .fieldset-wrapper').slideDown();
            $('#webform-component-personal-info-header > h3').removeClass('inactive').addClass('active');
            $('#webform-component-personal-info--next').show();
            $('#webform-component-payment-header').show();


            $('#plans').load(basePath + 'ajax/update-plans', {path: nid}, function () {
                $('#webform-component-1-upgrade-your-coverage .fieldset-wrapper').show();
                $('#webform-component-1-upgrade-your-coverage .add-btn').click(function () {
                    $('#summary-box').fadeOut().fadeIn();
                    var plan = $(this).attr('nid');
                    var price = $(this).attr('price');
                    var pathname = $(location).attr('pathname');
                    pathname_array = pathname.split('/');
                    path = pathname_array[3];

                    //Move to the next step
                    $('#webform-component-1-upgrade-your-coverage .fieldset-wrapper').slideUp();
                    $('#webform-component-upgrade-header > h3').removeClass('active').addClass('inactive processed');
                    $('#webform-component-personal-info .fieldset-wrapper').slideDown();
                    $('#webform-component-personal-info-header > h3').removeClass('inactive').addClass('active');
                    $('#webform-component-personal-info--next').show();
                    $('#webform-component-payment-header').show();

                    //Update the summary box
                    $.getJSON(basePath + "ajax/update-summary", {path: path, plan: plan}, function (data) {
                        if (data.reg != 0) {
                            var firstMonth = +data.price + +data.reg;
                            $('#summary-box--reg').html('$' + parseFloat(data.reg).toFixed(2));
                            $('#summary-box-first-total').html('$' + firstMonth);
                            $('#summary-box li.registration').show();
                            $('#summary-box p.price-reg-total').show();
                            $('#sig-price-one').html(firstMonth);
                        }
                        else {
                            $('#sig-price-one').html(parseFloat(data.price).toFixed(2));
                            $('#summary-box li.registration').hide();
                            $('#summary-box p.price-reg-total').hide();
                        }
                        if (data.fee != 0) {
                            $('#summary-box--mem').html('$' + parseFloat(data.fee).toFixed(2));
                            $('#summary-box li.membership').show();
                        }
                        $('#summary-box-total').html('$' + parseFloat(data.price).toFixed(2));
                        $('#summary-plan').html(data.package_name);
                        //Update the Disclaimer
                        $('#sig-price-two').html(parseFloat(data.price).toFixed(2));
                        //Update the hidden form elements
                        $("input[name='submitted[price]']").val(price);
                        $("input[name='submitted[plan_nid]']").val(plan);
                        $("input[name='submitted[package]']").val(data.package_id);
                    });
                });

                $('#free-link').click(function () {
                    //Move to the next step
                    $('#webform-component-1-upgrade-your-coverage .fieldset-wrapper').slideUp();
                    $('#webform-component-upgrade-header > h3').removeClass('active').addClass('inactive processed');
                    $('#webform-component-personal-info .fieldset-wrapper').slideDown();
                    $('#webform-component-personal-info-header > h3').removeClass('inactive').addClass('active');
                    $('#webform-component-personal-info--next').hide();
                    $('#webform-component-payment-header').hide();
                    $('#summary-box').fadeOut();
                    $('#summary-plan').html("USAP Health Card with $3,000 AD&D");
                    $('#summary-box ul li.registration').hide();
                    $('#summary-box ul li.membership').hide();
                    $('#summary-box ul li.premium').hide();
                    $('#summary-box .price-reg-total').hide();
                    $('#summary-box-total').html("$0.00");
                    $('#summary-box').fadeIn();
                    $("input[name='submitted[price]']").val(0);
                    $("input[name='submitted[plan_nid]']").val(9);
                    $("input[name='submitted[package]']").val("CHUBB USAP Health Card with $3,000 AD&D");
                    $('#edit-actions--2').show();

                    //No payment, so no required fields
                    $('#webform-component-payment--credit-card .form-text').removeClass('required');
                    $('#webform-component-payment--credit-card .form-select').removeClass('required');
                    $('#webform-component-payment--check .form-text').removeClass('required');
                    $('#edit-submitted-payment-e-sig').removeClass('required');
                    return false;
                });

            });
            //Go to payment section
            $('.add-btn-next').click(function () {
                if (check_required('info')) {

                    $('#webform-component-personal-info > .fieldset-wrapper').slideUp();
                    $('#webform-component-personal-info-header').children('h3').removeClass('active').addClass('inactive processed');
                    $('#webform-component-payment .fieldset-wrapper').slideDown();
                    $('#webform-component-payment-header').children('h3').removeClass('inactive').addClass('active');
                    $('#edit-actions--2').show();

                    //Set "required" fields
                    $('#webform-component-payment--credit-card .form-text').addClass('required');
                    $('#webform-component-payment--credit-card .form-select').addClass('required');
                    $('#edit-submitted-payment-e-sig').addClass('required');
                }

            });


            $('#edit-submit').click(function () {
                $('#preloader').delay(1000).css({'backgroundColor': 'rgba(255,255,255,0.8)', 'display': 'block'});
                console.log(check_required('submit'));
                if (check_required('submit')) {
                    return true;
                }
                else {
                    return false;
                }
            });

            $('#edit-submit--2').click(function () {
                $('#preloader').delay(1000).css({'backgroundColor': 'rgba(255,255,255,0.8)', 'display': 'block'});
                //console.log(check_required('submit'));
                if (check_required('submit')) {
                    return true;
                }
                else {
                    $('#preloader').hide();
                    $('#webform-component-affinity-error').html('<strong>Please Check Required Fields</strong>');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    //console.log(check_required('submit'));
                    return false;
                }
            });


            $('#client_details_authorize_final').click(function () {
                console.log(check_required('submit'));
                if (check_required('submit')) {
                    return true;
                }
                else {
                    $('.danger.alert').show();
                    $('.danger.alert').html('<strong>Please be sure all agreements are checked and e-signature matches applicant name shown above. For assistance please call 888-424-4186.</strong>');
                    return false;
                }
            });

            //Credit card or check
            $('#webform-component-payment--check').hide();
            $('#webform-component-payment--billing-info').hide();
            $('.form-item-submitted-payment-payment-type input').change(function () {
                radio = ($(this).attr('id'));
                if (radio == 'edit-submitted-payment-payment-type-1') {
                    $("#webform-component-payment--check").hide();
                    $("#webform-component-payment--credit-card").show();
                    $('#webform-component-payment--credit-card .form-text').addClass('required');
                    $('#webform-component-payment--credit-card .form-select').addClass('required');
                    $('#webform-component-payment--check .form-text').removeClass('required');
                }
                else if (radio == 'edit-submitted-payment-payment-type-2') {
                    $("#webform-component-payment--check").show();
                    $("#webform-component-payment--credit-card").hide();
                    $('#webform-component-payment--credit-card .form-text').removeClass('required');
                    $('#webform-component-payment--credit-card .form-select').removeClass('required');
                    $('#webform-component-payment--check .form-text').addClass('required');
                }
            });
            //Different Billing information
            $('#edit-submitted-payment-billing-different-1').click(function () {
                $('#webform-component-payment--billing-info').toggle();
            });

        }
    }

    function check_required(stage) {
        var result = true;
        //Global reset
        $('.webform-client-form .form-text').attr('style', '');
        $('.webform-client-form .form-select').attr('style', '');
        $('.webform-client-form label.checkbox').attr('style', '');


        $(".required").each(function (i) {
            if ($(this).val() == '') {
                $(this).css('border', '2px solid red');
                result = false;
            }
        });


        var fname = $('.applicant-first strong').text().toLowerCase();
        var lname = $('.applicant-last strong').text().toLowerCase();
        //var signature = $('#authSig').val().toLowerCase();


        // if( signature != fname + ' ' + lname ){
        //      result = false;
        // }

        console.log($('select.year.form-select').val());
        if ($('select.year.form-select').val() == '') {
            $('select.year.form-select').css('border', '2px solid red');
            result = false;

        }


    //console.log($('select.year.form-select').val());
    if ($('select.month.form-select').val() == '') {
        $('select.month.form-select').css('border', '2px solid red');
        result = false;
    }

    //console.log($('select.year.form-select').val());
    if($('select.day.form-select').val() == '') {
        $('select.day.form-select').css('border', '2px solid red');
        result = false;
    }


    if($('.page-node-2278 #webform-component-personal-info--mail-or-email input[type=checkbox]').length > 0) {
    var checks = 0;
        $('.page-node-2278 #webform-component-personal-info--mail-or-email input[type=checkbox]').each(function (i) {
            if ($(this).is(':checked')) {
                console.log('Checked!');
                checks++;
            } else {
                console.log('No Checked!');

            }

        });
        console.log(checks);
        if(checks < 1){
            $('#webform-component-personal-info--mail-or-email label').css('border', '2px solid red');
            result = false;
        }else{
            $('#webform-component-personal-info--mail-or-email label').css('border', 'none');
        }
    }
    if( !isValidEmailAddress( $('#edit-submitted-personal-info-info-col-1-email').val() ) ) {
      $('#edit-submitted-personal-info-info-col-1-email').css('border', '2px solid red');
      //result = false;
    };
    if( !isValidZip( $('#edit-submitted-personal-info-zip').val() ) ) {
      //$('#edit-submitted-personal-info-info-col-2-zip').css('border', '2px solid red');
      //result = false;
    };

    return result;
  }

  //check if the zip is a 5 number zip code
  function isValidZip(zip) {
    var pattern = new RegExp(/^\d{5}$/);
    return pattern.test(zip);
    };

  //We need to check the email on the client side or we get a hidden error later
  function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
  };

  // Adds tracking to the upgrade buttons
  $(".price-option").each(function(){
    var text = $(this).text();
    var eventTracking = ", 'Button','Upgrade']);"
    $( this ).next().attr("onClick","_gaq.push(['_trackEvent'," + " ' " + text + " ' " + eventTracking );
  });

  // Adds tracking each section title on the form
  $("h3 .info-title").each(function(){
    var text = $(this).text();
    var eventTracking = ", 'Text','Section Title']);"
    $( this ).attr("onClick","_gaq.push(['_trackEvent'," + " ' " + text + " ' " + eventTracking );
  });

  //tracking for the free link
  $("#free-link").attr("onClick","_gaq.push(['_trackEvent', 'Text', 'Free Health Card', 'Opt Out']);");

  //tracking for the next button
  $(".add-btn-next").attr("onClick","_gaq.push(['_trackEvent', 'Button', 'Next', 'Continue Form']);");

  //tracking for the submit button
  $("#edit-submit--2").attr("onClick","_gaq.push(['_trackEvent', 'Button', 'Enroll', 'Submit Button']);");

})(jQuery);
