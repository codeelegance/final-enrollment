<?php

/**
* Menu callback for admin/config/content/webform.
*/
function create_dependents_admin_settings()
{
    $form['formid']  = array(
        '#type' => 'textfield',
        '#title' => t('Which Form to Add Depedents to'),
        '#default_value' => '',
    );

    $form['dependents']  = array(
        '#type' => 'textfield',
        '#title' => t('How many Dependents'),
        '#default_value' => '1',
        '#description' => t('How many Dependents to add to the Form ID'),
    );

    $form = system_settings_form($form);

    $form['#submit'][] = 'create_dependents_submit_handler';
    return $form;
}


function create_dependents_submit_handler($form, &$form_state){
var_dump($form_state['values']['formid']);
var_dump($form_state['values']['dependents']);

//lets try to programatically add some form fields
    $component['nid'] = $form_state['values']['formid'];
    $dependent_count = (int)$form_state['values']['dependents'];

    //add a dependent count hidden field
    $next_id_query = db_select('webform_component')->condition('nid', $component['nid']);
    $next_id_query->addExpression('MAX(cid) + 1', 'cid');
    $component['cid'] = $next_id_query->execute()->fetchField();

    $query = db_insert('webform_component')
        ->fields(array(
            'nid' => $component['nid'],
            'cid' => $component['cid'],
            'pid' => 0,
            'form_key' => 'dependent_count',
            'name' => 'dependent_count',
            'type' => 'hidden',
            'value' => '0',
            'extra' => serialize([
                'hidden_type' => 'hidden',
                'private' => 0,
                'conditional_operator' => '='
            ]),
            'mandatory' => 0,
            'weight' => '1000',
        ))
        ->execute();

    $textfield_cid = $component['cid'] + 1;
    $query = db_insert('webform_component')
        ->fields(array(
            'nid' => $component['nid'],
            'cid' => $textfield_cid,
            'pid' => 0,
            'form_key' => 'add_dependent',
            'name' => 'add dependent',
            'type' => 'markup',
            'value' => '<input class="add-dep" type="button" value="Add Dependent" />' ,
            'extra' => serialize([
                'hidden_type' => 'hidden',
                'format' => 'full_html',
                'private' => 0,
                'conditional_operator' => '='
            ]),
            'mandatory' => 0,
            'weight' => '999',
        ))
        ->execute();

    for($i = 1; $i <= $dependent_count; $i++){
        $next_id_query = db_select('webform_component')->condition('nid', $component['nid']);
        $next_id_query->addExpression('MAX(cid) + 1', 'cid');
        $component['cid'] = $next_id_query->execute()->fetchField();
//make a fieldset first then add all text fields to that
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $component['cid'],
                'pid' => 0,
                'form_key' => 'dependent_'.$i,
                'name' => 'Dependent',
                'type' => 'fieldset',
                'value' => ' ',
                'extra' => serialize([
                    'title_display' => 0,
                    'private' => 0,
                    'collapsible' => 0,
                    'collapsed' => 0,
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '1000',
            ))
            ->execute();


        //dependent name
        $parent_fieldset = $component['cid'];
        $textfield_cid = $component['cid'] + 1;
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $textfield_cid,
                'pid' => $parent_fieldset,
                'form_key' => 'dep_name'.$i,
                'name' => 'Dependent '.$i.' Name',
                'type' => 'textfield',
                'value' => '',
                'extra' => serialize([
                    'title_display' => 0,
                    'private' => 0,
                    'collapsible' => 0,
                    'collapsed' => 0,
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '0',
            ))
            ->execute();

        $textfield_cid = $textfield_cid + 1;
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $textfield_cid,
                'pid' => $parent_fieldset,
                'form_key' => 'dep_relation'.$i,
                'name' => 'Dependent '.$i.' Relation',
                'type' => 'select',
                'value' => '',
                'extra' => serialize([
                    'items' => 'Spouse|Spouse
                    Child|Child
                    Parent|Parent
                    Other|Other',
                    'title_display' => 0,
                    'private' => 0,
                    'collapsible' => 0,
                    'aslist'=>1,
                    'collapsed' => 0,
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '1',
            ))
            ->execute();

        $textfield_cid = $textfield_cid + 1;
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $textfield_cid,
                'pid' => $parent_fieldset,
                'form_key' => 'dep_gender'.$i,
                'name' => 'Dependent '.$i.' Gender',
                'type' => 'select',
                'value' => '',
                'extra' => serialize([
                    'items' => 'Male|Male
                    Female|Female',
                    'title_display' => 0,
                    'private' => 0,
                    'collapsible' => 0,
                    'aslist'=>0,
                    'collapsed' => 0,
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '2',
            ))
            ->execute();

        $textfield_cid = $textfield_cid + 1;
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $textfield_cid,
                'pid' => $parent_fieldset,
                'form_key' => 'dep_birthday'.$i,
                'name' => 'Dependent '.$i.' Birthday',
                'type' => 'date',
                'value' => '',
                'extra' => serialize([
                    'time_zone'=>'user',
                    'title_display'=>'before',
                    'private'=>0,
                    'datepicker'=>0,
                    'year_textfield'=>0,
                    'start_date'=>'-75 years',
                    'end_date'=>'+0 years',
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '3',
            ))
            ->execute();

        $textfield_cid = $textfield_cid + 1;
        $query = db_insert('webform_component')
            ->fields(array(
                'nid' => $component['nid'],
                'cid' => $textfield_cid,
                'pid' => $parent_fieldset,
                'form_key' => 'add_dependent',
                'name' => 'add dependent',
                'type' => 'markup',
                'value' => '<input class="remove-dep" data-dependent="'.$i.'" type="button" value="Remove Dependent" />' ,
                'extra' => serialize([
                    'hidden_type' => 'hidden',
                    'format' => 'full_html',
                    'private' => 0,
                    'conditional_operator' => '='
                ]),
                'mandatory' => 0,
                'weight' => '4',
            ))
            ->execute();

    }

}