<?php
// display errors (just for debug mode)
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

// include drupal API
chdir("../../../../../../");
define('DRUPAL_ROOT', getcwd());
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// print_r($_POST);
// print_r($_FILES);

$email = $_POST['email'];
// $logo = $_POST['logo'];
$action_url = $_POST['actionURL'];
$packages = $_POST['packages'];
$module_path = drupal_get_path('module', 'form_builder');
$html = '<form action="#">' . $_POST['formHTML'] . '</form>
         <script src="'.$module_path.'/js/submit-form.js"></script>
         <script src="'.$module_path.'/js/jquery/2.1.4/jquery.min.js"></script>';

// <link rel="stylesheet" href="'.$module_path.'/css/form-style.css"/>
// <link rel="stylesheet" href="'.$module_path.'/css/mystyle.css"/>

// save the node
save_node();
echo 'The form has been saved successfully !';

function save_node() {
  // The form node
  $values = array(
    'type' => 'page',
    'uid' => $GLOBALS['user']->uid,
    'status' => 1,
    'comment' => 0,
    'promote' => 0,
  );
  $entity = entity_create('node', $values);

  // Create an entity_metadata_wrapper around the new node entity
  $ewrapper = entity_metadata_wrapper('node', $entity);
  $ewrapper->title->set($_POST['title']);
  $ewrapper->body->set(array('value' => $GLOBALS['html']));
  $ewrapper->body->summary->set($_POST['summary']);
  $ewrapper->body->format->set('full_html');

  // Setting the value of an entity reference field only requires passing
  // the entity id (e.g., nid) of the entity to which you want to refer
  // The nid 20 here is just an example.
  // **** $ref_nid = 20;
  // **** $ewrapper->field_my_entity_ref->set(intval($ref_nid));

  $my_date = new DateTime('January 1, 2017');
  $entity->field_my_date[LANGUAGE_NONE][0] = array(
     'value' => date_format($my_date, 'Y-m-d'),
     'timezone' => 'UTC',
     'timezone_db' => 'UTC',
   );

  $ewrapper->save();
}

?>
