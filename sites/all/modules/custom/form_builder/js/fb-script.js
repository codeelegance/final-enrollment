jQuery(function($) {
  var $fbEditor = $(document.getElementById('fb-editor')),
    $formContainer = $(document.getElementById('fb-rendered-form')),
    inputSets = [{
        label: 'User Details',
        name: 'user-details', // optional
        showHeader: true, // optional
        fields: [{
          type: 'text',
          label: 'First Name',
          className: 'form-control'
        }, {
          type: 'select',
          label: 'Profession',
          className: 'form-control',
          values: [{
            label: 'profession2',
            value: 'option-2',
            selected: false
          }, {
            label: 'profession3',
            value: 'option-3',
            selected: false
          }]
        }, {
          type: 'textarea',
          label: 'Short Bio:',
          className: 'form-control'
        }]
      }, {
        label: 'User Agreement',
        fields: [{
          type: 'header',
          subtype: 'h3',
          label: 'Terms & Conditions',
          className: 'header'
        }, {
          type: 'paragraph',
          label: 'Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.',
        }, {
          type: 'paragraph',
          label: 'Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.',
        }, {
          type: 'checkbox',
          label: 'Do you agree to the terms and conditions?',
        }]
      }],
    fbOptions = {
      onSave: function() {
        $fbEditor.toggle()
        $formContainer.toggle()
        $('form', $formContainer).formRender({
          formData: formBuilder.formData
        })
      },
      inputSets: inputSets
    },
    formBuilder = $fbEditor.formBuilder(fbOptions)

  $('#edit-form', $formContainer).click(function() {
    $fbEditor.toggle()
    $formContainer.toggle()
  })

  $('#submit-form').click(function() {
    var url = 'sites/all/modules/custom/form_builder/php/save_node.php'
    var data = new FormData()
    data.append('title', $('#fb-header #title').val())
    data.append('email', $('#fb-header #email').val())
    data.append('logo', $('#fb-header #logo-img').prop('files')[0])
    data.append('actionURL', $('#fb-header #actionURL').val())
    data.append('packages', $('#fb-header #summary-packages').val())
    data.append('summary', $('#fb-header #summary-body').val())
    data.append('formHTML', $('form').html())

    var result = $.ajax({
      url: url,
      type: 'POST',
      data: data,
      // dataType : 'json',
      cache: false,
      contentType: false,
      processData: false,
      timeout: 5000,
    }).done(function(res, status, jqXHR) {
      alert('done: ' + res)
    }).fail(function(res, status, err) {
      alert('fail: ' + err)
    }).always(function() {})
  })
})


// jQuery(window).on('load', function() {
//     document.getElementById("getStylesheet").addEventListener('click', function(event) {
//       var styleSheetList = document.styleSheets;
//       var css = [].slice.call(styleSheetList[6].cssRules)
//           .reduce(function (prev, cssRule) {
//               return prev + cssRule.cssText;
//           }, '');
//         $('#css').attr('class', 'after').html(css)
//     })
// })
