<?php

/**
 *Enrollment configuration form
 **/
function enrollment_admin_settings() {
	global $base_url;
	$form = array();
	$form['admin_enrollment_url'] = array(
		'#type' => 'textfield',
		'#title' => t('URL for the admin system'),
		'#description' => t('Make sure to include protocol (https://, http://, etc.)'),
		'#default_value' => variable_get('admin_enrollment_url', 'https://portal.nbfsa.com/post_operation.html'),
		'#required' => TRUE
	);

	$form['enrollment_billing_frequency'] = array(
		'#type' => 'textfield',
		'#title' => t('Default Billing Frequency'),
		'#default_value' => variable_get('enrollment_billing_frequency', 'MONTHLY'),
		'#required' => TRUE
	);
	$form['enrollment_return_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Return URL for enrollments'),
		'#default_value' => variable_get('enrollment_return_url', $base_url . '/enrollment-return/'),
		'#required' => TRUE
	);
	return system_settings_form($form);
}

function merge_users_submit_handler($form, &$form_state){

    var_dump($form);
    die();
    return "Test Return";
}

/**
 * Menu callback for admin/config/content/webform.
 */
function enrollment_merge_users()
{
db_set_active('web2');



  $result = db_query("SELECT * FROM users");

  //switch back to main site db
  db_set_active();

  foreach ($result as $record) {
    // Do something with each $record
      //var_dump($record->mail);
    $main_db_users = db_query("SELECT * FROM users WHERE mail = '$record->mail'");
    //var_dump($record->mail);
    $record_main = $main_db_users->fetchAssoc();
    //check if mail record was found if not we need to create user in new site and pull fields associated with that user

    //check false

    //find fields for user

      //save user with proper credentials and new uid

if($record_main == false) {
//    var_dump($record);
//    die();

        db_set_active('web2');
        $user_info = user_load($record->uid);
        //var_dump($user_info);

        db_set_active();
        user_save(
            '', [
                'name' => $user_info->name,
                'pass' => $user_info->pass,
                'mail' => $user_info->mail,
                'status' => 1,
                'init' => '',
                'roles' => [4 => true],
                'field_agents_package' => [
                    'und' => [
                        0 => [
                            'value' => $user_info->field_agents_package['und'][0]['value']
                        ]
                    ]
                ],
                'field_tree' => [
                    'und' => [
                        0 => [
                            'value' => $user_info->field_tree['und'][0]['value']
                        ]
                    ]
                ]
            ]
        );

    //var_dump($record);
}
  }

  //$amanfu = user_load(913);
  //var_dump($amanfu);


}